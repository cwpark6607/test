var express = require('express');
var router = express.Router();
//var bodyParser = require('body-parser');
var fs = require('fs');
//var ejs = require('ejs');

//router.use(bodyParser.urlencoded({extended: false}));

router.get('/', (req, res) => {
    res.redirect('./main');
});

router.get('/main', (req, res) => {
    console.log('main page');

    fs.readFile('./html/main.html', 'utf-8', (err, data) => {
        res.send(data);
    });
});

module.exports = router;